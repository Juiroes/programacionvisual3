﻿namespace RelojChecador
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEmpleado = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.gbBotones = new System.Windows.Forms.GroupBox();
            this.rbtnSalida = new System.Windows.Forms.RadioButton();
            this.rtbnEntrada = new System.Windows.Forms.RadioButton();
            this.btnIngresar = new System.Windows.Forms.Button();
            this.btnRevisar = new System.Windows.Forms.Button();
            this.gbBotones.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblEmpleado
            // 
            this.lblEmpleado.AutoSize = true;
            this.lblEmpleado.Location = new System.Drawing.Point(70, 94);
            this.lblEmpleado.Name = "lblEmpleado";
            this.lblEmpleado.Size = new System.Drawing.Size(60, 13);
            this.lblEmpleado.TabIndex = 0;
            this.lblEmpleado.Text = "Empleado: ";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Location = new System.Drawing.Point(70, 171);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(63, 13);
            this.lblFecha.TabIndex = 2;
            this.lblFecha.Text = "Fecha/hora";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Juan",
            "Isaac",
            "Velez",
            "Omar",
            "Aldo",
            "Aza",
            "Inge",
            "Puga",
            "Urrutia"});
            this.comboBox1.Location = new System.Drawing.Point(151, 86);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(314, 21);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.Text = "Seleccione";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(153, 164);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(312, 20);
            this.dateTimePicker1.TabIndex = 4;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // gbBotones
            // 
            this.gbBotones.Controls.Add(this.rbtnSalida);
            this.gbBotones.Controls.Add(this.rtbnEntrada);
            this.gbBotones.Location = new System.Drawing.Point(152, 236);
            this.gbBotones.Name = "gbBotones";
            this.gbBotones.Size = new System.Drawing.Size(312, 68);
            this.gbBotones.TabIndex = 5;
            this.gbBotones.TabStop = false;
            this.gbBotones.Text = "Operacion";
            // 
            // rbtnSalida
            // 
            this.rbtnSalida.AutoSize = true;
            this.rbtnSalida.Location = new System.Drawing.Point(161, 27);
            this.rbtnSalida.Name = "rbtnSalida";
            this.rbtnSalida.Size = new System.Drawing.Size(54, 17);
            this.rbtnSalida.TabIndex = 1;
            this.rbtnSalida.TabStop = true;
            this.rbtnSalida.Text = "Salida";
            this.rbtnSalida.UseVisualStyleBackColor = true;
            this.rbtnSalida.CheckedChanged += new System.EventHandler(this.rbtnSalida_CheckedChanged);
            // 
            // rtbnEntrada
            // 
            this.rtbnEntrada.AutoSize = true;
            this.rtbnEntrada.Location = new System.Drawing.Point(22, 27);
            this.rtbnEntrada.Name = "rtbnEntrada";
            this.rtbnEntrada.Size = new System.Drawing.Size(63, 17);
            this.rtbnEntrada.TabIndex = 0;
            this.rtbnEntrada.TabStop = true;
            this.rtbnEntrada.Text = "Ingresar";
            this.rtbnEntrada.UseVisualStyleBackColor = true;
            this.rtbnEntrada.CheckedChanged += new System.EventHandler(this.rtbnEntrada_CheckedChanged);
            // 
            // btnIngresar
            // 
            this.btnIngresar.Enabled = false;
            this.btnIngresar.Location = new System.Drawing.Point(163, 337);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(83, 36);
            this.btnIngresar.TabIndex = 6;
            this.btnIngresar.Text = "Registrar";
            this.btnIngresar.UseVisualStyleBackColor = true;
            this.btnIngresar.Click += new System.EventHandler(this.btnIngresar_Click);
            // 
            // btnRevisar
            // 
            this.btnRevisar.Enabled = false;
            this.btnRevisar.Location = new System.Drawing.Point(303, 337);
            this.btnRevisar.Name = "btnRevisar";
            this.btnRevisar.Size = new System.Drawing.Size(83, 36);
            this.btnRevisar.TabIndex = 7;
            this.btnRevisar.Text = "Revisar";
            this.btnRevisar.UseVisualStyleBackColor = true;
            this.btnRevisar.Click += new System.EventHandler(this.btnRevisar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnRevisar);
            this.Controls.Add(this.btnIngresar);
            this.Controls.Add(this.gbBotones);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.lblEmpleado);
            this.Name = "Form1";
            this.Text = "Reloj";
            this.gbBotones.ResumeLayout(false);
            this.gbBotones.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblEmpleado;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.GroupBox gbBotones;
        private System.Windows.Forms.RadioButton rbtnSalida;
        private System.Windows.Forms.RadioButton rtbnEntrada;
        private System.Windows.Forms.Button btnIngresar;
        private System.Windows.Forms.Button btnRevisar;
    }
}

