﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; 

namespace RelojChecador
{
    public partial class Form1 : Form
    {
        //crea objetos de tipo fecha
        DateTime datoEntrada = new DateTime();
        DateTime vaciarDato = new DateTime();
        DateTime datoSalida = new DateTime();
        //crea esta variable de prueba
        string empleadoComboBox;

        //banderas de prueba
        bool v = false;
        bool v1 = false;
        int flag = 0;

        string Path = @"C:\Users\juiro\desktop\Examen.txt";
        


        public Form1()
        {
            InitializeComponent();
        }
       

        private void btnIngresar_Click(object sender, EventArgs e)
        {
           empleadoComboBox = comboBox1.Text;

            if (!File.Exists(Path))
            {
                using (StreamWriter sw = File.CreateText(Path))
                {
                    sw.WriteLine(empleadoComboBox);
                    sw.WriteLine(datoEntrada);
                    
                    }
            }
            else
            {
                string append="";
                if(flag == 1)
                {
                     append = "Entrada\n"+ empleadoComboBox + "\n" + datoEntrada + Environment.NewLine;
                }else if(flag ==2)
                {
                     append = "Salida\n" + empleadoComboBox + "\n" + datoSalida + Environment.NewLine;
                }
               
                File.AppendAllText(Path, append);
                datoEntrada = vaciarDato;
                
            }



        }

        private void btnRevisar_Click(object sender, EventArgs e)
        {

            string readText = File.ReadAllText(Path);
            MessageBox.Show(readText);


        }

        private void rtbnEntrada_CheckedChanged(object sender, EventArgs e)
        {
            datoEntrada = dateTimePicker1.Value;
            btnIngresar.Enabled = true;
            btnRevisar.Enabled = true;
            v = true;
            flag = 1;
        }

        private void rbtnSalida_CheckedChanged(object sender, EventArgs e)
        {
            datoSalida = dateTimePicker1.Value;
            btnIngresar.Enabled = true;
            btnRevisar.Enabled = true;
            v1 = true;
            flag = 2;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            datoEntrada = dateTimePicker1.Value;
            datoSalida = dateTimePicker1.Value;
        }
    }
}
    