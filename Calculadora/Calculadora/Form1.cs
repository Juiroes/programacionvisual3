﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private double numeroUno=0;
        private double numeroDos=0;
        private double resultado;
        bool flag = false; 
        int op = 0;

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSeven_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "7";
            flag = true;
            
        }
            
        private void btnEight_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "8";
            flag = true;
        }

        private void btnNine_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "9";
            flag = true;
        }

        private void MoreOrLess_click(object sender, EventArgs e)
        {
            txtResultado.Text += "-";
            flag = true;

        }

        private void btnFour_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "4";
            flag = true;

        }

        private void btnFive_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "5";
            flag = true;

        }

        private void btnSix_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "6";
            flag = true;

        }

        private void btnOne_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "1";
            flag = true;

        }

        private void btnTwo_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "2";
            flag = true;

        }

        private void btnThree_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "3";
            flag = true;

        }

        private void btnCero_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "0";
            flag = true;

        }

        private void btnPunto_Click(object sender, EventArgs e)
        {
            if (!(txtResultado.Text.Contains(".")))
            {
                txtResultado.Text += ".";
                flag = true;
            }
            else
            {
                txtResultado.Text.Remove(txtResultado.Text.Length-1);
                
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            
            if (txtResultado.Text == String.Empty)
            {
                MessageBox.Show("no puede borrar si no hay nada");
                flag = false;
            }
            else
            {
                if (txtResultado.Text.Contains("0") || txtResultado.Text.Contains("1") || txtResultado.Text.Contains("2")
                 || txtResultado.Text.Contains("3") || txtResultado.Text.Contains("4") || txtResultado.Text.Contains("5") ||
                 txtResultado.Text.Contains("6") || txtResultado.Text.Contains("7") || txtResultado.Text.Contains("8") ||
                 txtResultado.Text.Contains("9") || txtResultado.Text.Contains("-") || txtResultado.Text.Contains("."))
                {
                    txtResultado.Text = txtResultado.Text.Remove((txtResultado.Text.Length - 1));

                    if (txtResultado.Text == String.Empty)
                    {
                        flag = false;
                    }
                   
                }
            }
            

        }
           

        private void btnSuma_Click(object sender, EventArgs e)
        {


            if (flag)
            {
                resultado = numeroUno;
                numeroUno = resultado+ double.Parse(txtResultado.Text);
                //numeroUno = double.Parse(txtResultado.Text);
                txtResultado.Clear();
                op = 1;
            }
            else
            {
                MessageBox.Show("No puede realizar una operacion si esta vacio");
            }
                


        }

        private void btnResta_Click(object sender, EventArgs e)
        {
            if (flag)
            {
                numeroUno = resultado - double.Parse(txtResultado.Text);
                //numeroUno = double.Parse(txtResultado.Text);
                txtResultado.Clear();
                op = 2;
            }
            else
            {
                MessageBox.Show("No puede realizar una operacion si esta vacio");
            }
        }

        private void btnProducto_Click(object sender, EventArgs e)
        {
            if (flag)
            {
                numeroUno = resultado + double.Parse(txtResultado.Text);
               // numeroUno = double.Parse(txtResultado.Text);
                txtResultado.Clear();
                op = 3;
            }
            else
            {
                MessageBox.Show("No puede realizar una operacion si esta vacio");
            }
        }

        private void btnCociente_Click(object sender, EventArgs e)
        {
            if (flag)
            {
                 numeroUno = resultado + double.Parse(txtResultado.Text);
                //numeroUno = double.Parse(txtResultado.Text);
                txtResultado.Clear();
                op = 4;
            }
            else
            {
                MessageBox.Show("No puede realizar una operacion si esta vacio");
            }
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {
            switch (op)
            {
                case 1:
                        
                    numeroDos = double.Parse(txtResultado.Text);

                    resultado = numeroUno + numeroDos;

                    txtResultado.Text = "" + resultado;
                    flag = true;

                break;
                case 2:

                    numeroDos = double.Parse(txtResultado.Text);

                    resultado = numeroUno - numeroDos;

                    txtResultado.Text = "" +  resultado;
                    break;
                case 3:

                    numeroDos = double.Parse(txtResultado.Text);

                    resultado = numeroUno * numeroDos;

                    txtResultado.Text = "" + resultado;
                    break;
                case 4:

                    numeroDos = double.Parse(txtResultado.Text);

                    if (numeroUno == 0 || numeroDos == 0 || numeroUno == 0 && numeroDos == 0)
                    {

                        resultado = numeroUno / numeroDos;

                        txtResultado.Text = "" + resultado;
                    }
                    else
                    {
                        MessageBox.Show("La division entre cero no es posible");
                    }
                    break;
            }
            numeroUno = 0;
            numeroDos = 0;
            resultado = 0;
        }
    }
}
